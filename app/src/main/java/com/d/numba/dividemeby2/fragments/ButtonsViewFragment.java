package com.d.numba.dividemeby2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.d.numba.dividemeby2.R;

import static com.d.numba.dividemeby2.FunctionsNeeded.animateButton;
import static com.d.numba.dividemeby2.FunctionsNeeded.setButtonBackground;

/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonsViewFragment extends Fragment {


    public ButtonsViewFragment() {
        // Required empty public constructor
    }

    TextView response;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_down_items_container, container, false);

        LinearLayout buttonContainer = (LinearLayout) rootView.findViewById(R.id.itemContainer);
        response = (TextView)getActivity().findViewById(R.id.result);
        for (int i = 0; i < 11; i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            params.setMargins(5, 5, 5, 5);
            final Button button = new Button(getActivity());
            button.setId(i);
            button.setLayoutParams(params);
            setButtonBackground(button,i,getActivity());
            button.setText("" + (i+1));
            if (i == 9)
                button.setText("" + (0));
            buttonContainer.addView(button);
            final int buttonValue = Integer.parseInt(button.getText().toString());

            if (i == 10) {
                button.setText("x");
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        response.setText("" + 0);
                        animateButton(button,getActivity());
                    }
                });
            } else {
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        animateButton(button,getActivity());
                        String resultText = response.getText().toString();
                        if (resultText.equals("0")) {
                            resultText = "";
                        }
                        response.setText(resultText + buttonValue);
                    }
                });
            }
        }

        return rootView;
    }

}
