package com.d.numba.dividemeby2;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.realm.Realm;

import static com.d.numba.dividemeby2.GameActivity.bestScoreValue;
import static com.d.numba.dividemeby2.GameActivity.goodAnswer;
import static com.d.numba.dividemeby2.GameActivity.isNeeded;
import static com.d.numba.dividemeby2.GameActivity.lost;
import static com.d.numba.dividemeby2.GameActivity.positionStart;


/**
 * by numba on 15/05/17.
 */

public class FunctionsNeeded {

    private static boolean shape1 = false;
    private static boolean shape2 = false;
    private static boolean shape3 = false;
    private static boolean shape4 = false;
    private static Realm realm ;

    //----------------------------------------- RESULT ANIMATION -----------------------------------

    public static void animateResult(final View response, final CustomBundleView bounce0, final Context context, final float mScreenWidth, final RelativeLayout mFrameLayout, final TextView score, final boolean gotRight, final View inflater, final Activity activity, final Intent intent, final boolean status, final InterstitialAd mInterstitialAd) {

        int originalPos[] = new int[2];
        response.getLocationOnScreen(originalPos);

        float xDest = bounce0.getView().getX();
        float yDest = bounce0.getView().getY();

        TranslateAnimation anim = new TranslateAnimation(0, xDest - originalPos[0], 0, yDest - originalPos[1]);
        anim.setDuration(500);
        anim.setFillAfter(true);
        response.startAnimation(anim);
        response.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                animation.setInterpolator(new ReverseInterpolator());
                if(gotRight) {
                    TextView t = new TextView(context);
                    GameActivity.goodAnswer = GameActivity.goodAnswer + 1;
                    t.setText("+1");
                    t.setTextSize(25);
                    int x = (int) bounce0.getView().getX();
                    int y = (int) bounce0.getView().getY();
                    t.setX(x);
                    t.setY(y);
                    if (status)
                    bounce0.animateBundleFaster((positionStart - x),mScreenWidth);
                    //else bounce0.resumeAnimationBundle((positionStart - x),mScreenWidth,DEFAULT_ANIMATION_DURATION);
                    mFrameLayout.addView(t);
                    animatePlus1(context,t,mFrameLayout,score,activity);
                }else {
                    ((GameActivity)activity).quitOne();
                    showPopup(activity,inflater,intent,mInterstitialAd);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    //----------------------------------------------- PLUS 1 ANIMATION -----------------------------

    private static void animatePlus1(final Context context, final View view, final RelativeLayout mFrameLayout, final TextView score, final Activity activity){
        int originalPos[] = new int[2];
        view.getLocationOnScreen( originalPos );

        float xDest = score.getX();
        float yDest = score.getY();

        TranslateAnimation anim = new TranslateAnimation( 0, xDest - originalPos[0] , 0, yDest - originalPos[1] );
        anim.setDuration(1000);
        anim.setFillAfter( true );

        view.startAnimation(anim);
        view.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mFrameLayout.removeView(view);
                animScore(context, score,activity);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    //-------------------------------------------------- BACKGROUND ANIMATION ----------------------

    public static void animateBackground(long FAST_ANIMATION_DURATION,Random rand,Context context,RelativeLayout mFrameLayout) {

        List<Integer> l = setBackgroundOnActivity(rand);
        int valFrom = l.get(0);
        int valTo = l.get(1);

        ObjectAnimator objectAnimator = ObjectAnimator.ofObject(mFrameLayout, "backgroundColor",
                new ArgbEvaluator(),
                ContextCompat.getColor(context, valFrom),
                ContextCompat.getColor(context, valTo));

        objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
        objectAnimator.setRepeatMode(ValueAnimator.REVERSE);

        objectAnimator.setDuration(FAST_ANIMATION_DURATION+1500L);
        objectAnimator.start();

    }

    //------------------------------------ SET BACKGROUND ON BACKGROUND ANIMATION ------------------

    private static List<Integer> setBackgroundOnActivity(Random rand){
        int valFrom = rand.nextInt(4);
        int valTo = rand.nextInt(4);
        List<Integer> list = new ArrayList<>();


        if(valFrom == 0){
            valFrom = R.color.background_from;
        }else if(valFrom == 1){
            valFrom = R.color.background_to;
        }
        else if(valFrom == 2){
            valFrom = R.color.colorPrimaryDark;
        }else if(valFrom == 3){
            valFrom = R.color.colorAccent;
        }
        if(valTo == 0){
            valTo = R.color.black_overlay;
        }else if(valTo == 1){
            valTo = R.color.colorPrimary;
        }
        else if(valTo == 2){
            valTo = R.color.range;
        }else if(valTo == 3){
            valTo = R.color.yel;
        }
        list.add(valFrom);
        list.add(valTo);
        return list;
    }

    //------------------------------------ SET BACKGROUND ON BUTTON --------------------------------

    public static void setButtonBackground(Button but, int position, Context context){
        if (position == 0) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b1));
        else if (position == 1) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b2));
        else if (position == 2) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b3));
        else if (position == 3) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b4));
        else if (position == 4) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b5));
        else if (position == 5) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b6));
        else if (position == 6) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b7));
        else if (position == 7) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b8));
        else if (position == 8) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b9));
        else if (position == 9) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b10));
        else if (position == 10) but.setBackgroundColor(ContextCompat.getColor(context,R.color.b11));
    }

    //------------------------------------ SET BACKGROUND ON COLORS --------------------------------

    public static void setColorBackground(ImageButton but, Context context){
        Random random = new Random();
        boolean done = false;

        while (!done){
            int value = random.nextInt(16);
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (value <= 4 && !shape1) {
                if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                    but.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_color1));
                else
                but.setBackground(context.getDrawable(R.drawable.shape_color1));
                shape1 = true;
                but.setTag("1");
                done = true;
            } else if ((value > 4 && value <= 8) && !shape2 ) {
                if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                    but.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_color2));
                else
                    but.setBackground(context.getDrawable(R.drawable.shape_color2));
                shape2 = true;
                but.setTag("2");
                done = true;
            } else if ((value > 8 && value <= 12) && !shape3) {
                if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                    but.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_color3));
                else
                    but.setBackground(context.getDrawable(R.drawable.shape_color3));
                shape3 = true;
                but.setTag("3");
                done = true;
            } else if (value > 12 && !shape4) {
                if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                    but.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_color4));
                else
                    but.setBackground(context.getDrawable(R.drawable.shape_color4));
                shape4 = true;
                but.setTag("4");
                done = true;
            }
        }
        if(shape1 && shape2 && shape3 && shape4){
            shape1 = false;
            shape2 = false;
            shape3 = false;
            shape4 = false;
        }

    }

    //------------------------------------------- GET BACK TO POSITION  ----------------------------

    private static class ReverseInterpolator implements Interpolator {
        @Override
        public float getInterpolation(float paramFloat) {
            return Math.abs(paramFloat -1f);
        }
    }

    //------------------------------------ ANIMATE COLORS ------------------------------------------

    public static void animateColor(final View imageColorView, final CustomBundleView bounce0, final Context context, final RelativeLayout mFrameLayout, final Activity activity) {

        final Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.zoomin);
        zoomIn.setDuration(250);
        final Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.zoomout);
        imageColorView.startAnimation(zoomIn);

        imageColorView.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationEnd(Animation animation) {
                imageColorView.setAnimation(zoomOut);
                animation.setInterpolator(new ReverseInterpolator());
                ViewPager viewPager;
                if(activity.getClass().equals(GameActivity.class))
                    viewPager = ((GameActivity)context).getViewPager();
                else
                    viewPager = ((HelpActivity)context).getPager();
                TextView t = new TextView(activity.getApplicationContext());
                t.setTextSize(25);
                if(bounce0.getView().getTag().equals(imageColorView.getTag())) {
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                        bounce0.getView().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape5));
                    else
                        bounce0.getView().setBackground(context.getDrawable(R.drawable.shape5));
                    float a = (float) 0.5;
                    GameActivity.goodAnswer = GameActivity.goodAnswer + a;
                    t.setText("+0,5");

                    ((TextView)bounce0.getView()).setTextColor(ContextCompat.getColor(context,R.color.emptybundle));
                    viewPager.setCurrentItem(1);
                }else {
                    float a = (float) 0.5;
                    GameActivity.goodAnswer = GameActivity.goodAnswer - (a);
                    t.setText("-0,5");
                }
                int x = (int) bounce0.getView().getX();
                int y = (int) bounce0.getView().getY();
                t.setX(x);
                t.setY(y);
                mFrameLayout.addView(t);
                if(activity.getClass().equals(GameActivity.class))
                animatePlus1(context,t,mFrameLayout,((GameActivity)activity).getScore(),activity);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    //-------------------------------------------------- ANIMATE SCORE  ----------------------------

    public static void animScore(Context context, final View view, final Activity activity){

        Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.zoomin);
        zoomIn.setDuration(250);
        final Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.zoomout);
        zoomIn.setDuration(250);
        view.setAnimation(zoomIn);
        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setAnimation(zoomOut);
                ((GameActivity)activity).getScore().setText(""+goodAnswer);
                isNeeded = false;
                if(goodAnswer < 0) {
                    ((GameActivity)activity).quitOne();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    //--------------------------------------- ANIMATE BUTTONS --------------------------------------

    public static void animateButton(final View button,final Context context) {


        final Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.zoomin);
        zoomIn.setDuration(100);
        final Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.zoomout);
        button.startAnimation(zoomIn);

        button.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }
            @Override
            public void onAnimationEnd(Animation animation) {
                button.setAnimation(zoomOut);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    //-------------------------------------- SHOW POPUP --------------------------------------------

    public static void showPopup(final Activity activity, View layout, final Intent intent, InterstitialAd mInterstitialAd) {
        try {
            final PopupWindow pw = new PopupWindow(layout, 600, 500, true);
            TextView textView = (TextView) layout.findViewById(R.id.finalScore);
            ImageButton restart = (ImageButton)layout.findViewById(R.id.restart);
            restart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pw.dismiss();
                    recreateActivityCompat(activity);
                }
            });
            textView.setText(""+goodAnswer);
            ImageButton home = (ImageButton) layout.findViewById(R.id.quitPop);
            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pw.dismiss();
                    activity.finish();
                    activity.startActivity(intent);
                }
            });
            TextView best = (TextView)layout.findViewById(R.id.bestPopup);
            best.setText(""+bestScoreValue);
            pw.showAtLocation(activity.findViewById(R.id.container), Gravity.CENTER, 0, 0);
            if(lost >= 5) {
                System.out.println("lost == 5");
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
                lost = 0;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------------------------------- CREATE,GET OR UPDATE BEST SCORE ----------------------

    public static BestScore getCreateOrUpdateBestScore(final float value,boolean getBestScore,Context context){

        Realm.init(context);
        realm = Realm.getDefaultInstance();

        final BestScore best = realm.where(BestScore.class).findFirst();
        if(!getBestScore) {
            if (best == null) {
                realm.beginTransaction();
                BestScore bestScore1 = realm.createObject(BestScore.class,1);
                bestScore1.setValue(value);
                realm.commitTransaction();
            } else if(value > best.getValue()){
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        best.setValue(value);
                        realm.copyToRealmOrUpdate(best);
                        bestScoreValue = value;
                    }
                });
            }
        }
        return best;
    }

    //--------------------------------------- CREATE,GET OR UPDATE BEST SCORE ----------------------

    public static UserSettings getCreateOrUpdateMusicStatus(final boolean value, final long speed, boolean getMusicStatus, Context context){

        Realm.init(context);
        realm = Realm.getDefaultInstance();

        final UserSettings best = realm.where(UserSettings.class).findFirst();
        if(!getMusicStatus) {
            if (best == null) {
                realm.beginTransaction();
                UserSettings musicStatus = realm.createObject(UserSettings.class,1);
                musicStatus.setMusicOn(value);
                musicStatus.setGameSpeed(6000L);
                realm.commitTransaction();
            } else if(value != best.isMusicOn()){
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        best.setMusicOn(value);
                        best.setGameSpeed(speed);
                        realm.copyToRealmOrUpdate(best);
                    }
                });
            }else if (value == best.isMusicOn()){
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        best.setMusicOn(value);
                        best.setGameSpeed(speed);
                        realm.copyToRealmOrUpdate(best);
                    }
                });
            }
        }
        return best;
    }

    //----------------------------------------- RECREATE ACTIVITY ----------------------------------

    public static void recreateActivityCompat(final Activity a) {
        ((GameActivity)a).getViewPager().setCurrentItem(0);
        ((GameActivity)a).wakeUp();
        goodAnswer = 0;
        a.recreate();
    }

    //---------------------------------------- ANIMATE ON PLACE ------------------------------------

    public static Animation animPlayButton(final View view){
        Animation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE,
                TranslateAnimation.ABSOLUTE,
                (int)view.getX(),
                (int)(view.getX())+50);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }


            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return  mAnimation;
    }

}
