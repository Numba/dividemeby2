package com.d.numba.dividemeby2;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * by numba on 18/05/17.
 */

public class UserSettings extends RealmObject {

    @PrimaryKey
    private int id;
    private boolean musicOn;
    private long gameSpeed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isMusicOn() {
        return musicOn;
    }

    void setMusicOn(boolean musicOn) {
        this.musicOn = musicOn;
    }

    public long getGameSpeed() {
        return gameSpeed;
    }

    void setGameSpeed(long gameSpeed) {
        this.gameSpeed = gameSpeed;
    }
}
