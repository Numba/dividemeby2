package com.d.numba.dividemeby2;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.d.numba.dividemeby2.fragments.ButtonsViewFragment;
import com.d.numba.dividemeby2.fragments.ColorsViewFragment;

import static com.d.numba.dividemeby2.FunctionsNeeded.animPlayButton;
import static com.d.numba.dividemeby2.FunctionsNeeded.getCreateOrUpdateMusicStatus;
import static com.d.numba.dividemeby2.Home.gameSound;

public class HelpActivity extends AppCompatActivity {

    private CustomBundleView bubble0;
    float limiti1 = 0,limite2 = 0;
    private ViewPager mPager;
    private RelativeLayout mFrameLayout;
    boolean dougou = false;
    private static final int NUM_PAGES = 2;
    private float mScreenWidth, mScreenHeight,positionStart;
    private boolean done = false;
    Context context;
    private TextView score;
    public static ImageView touchDown;
    static boolean touch = false;
    ImageView touchTop, slide;
    TextView bubble;
    Button line1,line2;

    public void quitOne() {
        done = true;
    }

    public boolean isDone() {
        return done;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        context = this;
        score = (TextView) findViewById(R.id.score);
        touchDown = (ImageView)findViewById(R.id.touchDownHelp);
        touchTop = (ImageView)findViewById(R.id.touchTopHelp);
        slide = (ImageView)findViewById(R.id.swipeHelp);
        bubble = (TextView)findViewById(R.id.buble);
        mFrameLayout = (RelativeLayout) findViewById(R.id.container);
        line1 = (Button)findViewById(R.id.limite1);
        line2 = (Button)findViewById(R.id.limite2);
        mPager = (NonSwipeViewPager) findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        getPager().setAdapter(mPagerAdapter);
        getPager().setPageTransformer(true, new ZoomOutPageTransformer());
        getPager().setCurrentItem(0);
        bubble0 = new CustomBundleView(bubble);
        bubble0.setBackgroundBubble(this);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    if (positionStart == 0) {
                        positionStart = getBubble0().getView().getX();
                        limiti1 = line1.getX();
                        limite2 = line2.getX();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                bubble0.animateBundle(mScreenWidth, 90000, HelpActivity.this);
            }
        }, 1020);
        ((new BundleTask())).execute(bubble0);
        touchDown.setAnimation(animPlayButton(touchDown));

    }

    public ViewPager getPager() {
        return mPager;
    }

    @Override
    protected void onResume() {
        super.onResume();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        mScreenWidth = displaymetrics.widthPixels;
        mScreenHeight = displaymetrics.heightPixels;
        if (getCreateOrUpdateMusicStatus(true, 0, true, context).isMusicOn()) gameSound.start();
    }

    public CustomBundleView getBubble0() {
        return bubble0;
    }

    public RelativeLayout getFrameLayout() {
        return mFrameLayout;
    }

    private class BundleTask extends AsyncTask<CustomBundleView, CustomBundleView, Void> {


        @Override
        public void onPreExecute() {

        }

        @Override
        protected Void doInBackground(CustomBundleView... arg0) {

            CustomBundleView v = arg0[0];
            while (!isDone()) {
                try {
                    Thread.sleep(10);
                        publishProgress(v);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(final CustomBundleView... values) {
            super.onProgressUpdate(values);
            float position = (positionStart - values[0].getView().getX());
            if(getBubble0().getView().getX() >= limiti1 - 10 && getBubble0().getView().getX() <= limiti1 + 10){
                touch = true;
                values[0].getAnimatorSetDefault().pause();
               // values[0].getView().clearAnimation();
            }
            if (position == mScreenWidth) {
            }
        }

        @Override
        protected void onPostExecute(Void result) {

        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return new ColorsViewFragment();
            else
                return new ButtonsViewFragment();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
