package com.d.numba.dividemeby2;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * by Numba on 16/05/17.
 */

public class BestScore extends RealmObject {

    @PrimaryKey
    private int id;
    private float value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    void setValue(float value) {
        this.value = value;
    }

}
