package com.d.numba.dividemeby2;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

import static com.d.numba.dividemeby2.FunctionsNeeded.animScore;
import static com.d.numba.dividemeby2.GameActivity.fast;
import static com.d.numba.dividemeby2.GameActivity.isNeeded;

/**
 * by numba on 12/05/17.
 */

public class CustomBundleView {
    private View view;
    private AnimatorSet animatorSetDefault;
    private AnimatorSet animatorSetFast;
    private static final long FAST_ANIMATION_DURATION = 500L;

    public CustomBundleView(View view) {
        this.setView(view);
        this.setAnimatorSetDefault(new AnimatorSet());
        this.setAnimatorSetFast(new AnimatorSet());
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public AnimatorSet getAnimatorSetDefault() {
        return animatorSetDefault;
    }

    public void setAnimatorSetDefault(AnimatorSet animatorSetDefault) {
        this.animatorSetDefault = animatorSetDefault;
    }

    //--------------------------------------- ANIMATE FASTER ---------------------------------------

    public void animateBundleFaster(float from,float mScreenWidth ) {

        ValueAnimator positionAnimator = ValueAnimator.ofFloat(-from, -mScreenWidth);

        positionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                getView().setTranslationX(value);
            }
        });


        ValueAnimator rotationAnimator = ValueAnimator.ofFloat(0, 360);

        rotationAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                getView().setRotation(value);
            }
        });


        setAnimatorSetFast(new AnimatorSet());
        getAnimatorSetFast().play(positionAnimator).with(rotationAnimator);
        getAnimatorSetFast().setDuration(FAST_ANIMATION_DURATION);
        getAnimatorSetFast().start();
        getAnimatorSetDefault().end();
        getAnimatorSetDefault().cancel();
        getAnimatorSetDefault().addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                fast = true;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    //--------------------------------------- DEFAULT ----------------------------------------------

    public void animateBundle(final float mScreenWidth, final long speed, final Activity activity) {

        ValueAnimator positionAnimator = ValueAnimator.ofFloat(0, -mScreenWidth);

        positionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                getView().setTranslationX(value);
            }
        });

        getAnimatorSetDefault().play(positionAnimator);
        getAnimatorSetDefault().setDuration(speed);
        getAnimatorSetDefault().start();
        getAnimatorSetDefault().addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                /*if(((GameActivity)activity).isFirst()) {
                    ((GameActivity) activity).start();
                    ((GameActivity) activity).setFirst(false);
                }*/
               // if (touch) {
//                    touchDown.startAnimation(animPlayButton(touchDown));
                //}
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(isNeeded) {
                   // animatePlus1(activity.getApplicationContext(), ((GameActivity)activity).getT(),((GameActivity)activity).getFrameLayout(),((GameActivity)activity).getScore(),activity);
                    animScore(activity.getApplicationContext(), ((GameActivity) activity).getScore(), activity);
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    //--------------------------------------- RESUME ANIMATION -------------------------------------

    public void resumeAnimationBundle(float from,final float mScreenWidth, final long speed) {

        ValueAnimator positionAnimator = ValueAnimator.ofFloat(-from, -mScreenWidth);

        positionAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue();
                getView().setTranslationX(value);
            }
        });

        getAnimatorSetDefault().play(positionAnimator);
        getAnimatorSetDefault().setDuration(speed);
        getAnimatorSetDefault().start();
    }

    //------------------------------------ SET BACKGROUND BUBBLE --------------------------------------------------

   // @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setBackgroundBubble(Context context){
        Random rand = new Random();
        int randomNum = rand.nextInt(3);
        ((TextView)getView()).setTextColor(ContextCompat.getColor(context,R.color.white));
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(randomNum == 0){
            if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                getView().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape1));
            else
            getView().setBackground(context.getDrawable(R.drawable.shape1));
            getView().setTag("1");
        }
        else if(randomNum == 1){
            if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                getView().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape2));
            else
                getView().setBackground(context.getDrawable(R.drawable.shape2));
            getView().setTag("2");
        }
        else if(randomNum == 2){
            if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                getView().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape3));
            else
                getView().setBackground(context.getDrawable(R.drawable.shape3));
            getView().setTag("3");
        }
        else if(randomNum == 3){
            if(sdk < android.os.Build.VERSION_CODES.LOLLIPOP)
                getView().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape4));
            else
                getView().setBackground(context.getDrawable(R.drawable.shape4));
            getView().setTag("4");
        }
    }

    public AnimatorSet getAnimatorSetFast() {
        return animatorSetFast;
    }

    public void setAnimatorSetFast(AnimatorSet animatorSetFast) {
        this.animatorSetFast = animatorSetFast;
    }
}
