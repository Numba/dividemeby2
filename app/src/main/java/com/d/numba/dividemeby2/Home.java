package com.d.numba.dividemeby2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Random;

import static android.R.drawable.ic_lock_silent_mode;
import static android.R.drawable.ic_lock_silent_mode_off;
import static com.d.numba.dividemeby2.FunctionsNeeded.animPlayButton;
import static com.d.numba.dividemeby2.FunctionsNeeded.animateBackground;
import static com.d.numba.dividemeby2.FunctionsNeeded.getCreateOrUpdateMusicStatus;
import static com.d.numba.dividemeby2.GameActivity.DEFAULT_ANIMATION_DURATION;
import static com.d.numba.dividemeby2.GameActivity.FAST_ANIMATION_DURATION;

public class Home extends AppCompatActivity {

    public static MediaPlayer gameSound;
    Context context;
    Random rand = new Random();
    private RelativeLayout mFrameLayout;

    private float mScreenWidth,mScreenHeight;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mFrameLayout = (RelativeLayout) findViewById(R.id.container);
        context = this;
        animateBackground(FAST_ANIMATION_DURATION, rand, context, mFrameLayout);
        if(gameSound == null) {
            gameSound = MediaPlayer.create(Home.this, R.raw.game);
            gameSound.setLooping(true);
            gameSound.start();
        }

        if(getCreateOrUpdateMusicStatus(true,0,true,this) == null ) {
            getCreateOrUpdateMusicStatus(true,DEFAULT_ANIMATION_DURATION,false,this);
        }else {
            if(!getCreateOrUpdateMusicStatus(true,0,true,this).isMusicOn())
               /* gameSound.start();
            else*/
                gameSound.pause();
        }
        DEFAULT_ANIMATION_DURATION = getCreateOrUpdateMusicStatus(true,0,true,this).getGameSpeed();
        ImageButton playNow = (ImageButton)findViewById(R.id.playNow);
        playNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, GameActivity.class);
                startActivity(intent);
                finish();
            }
        });
        playNow.setAnimation(animPlayButton(playNow));
        LayoutInflater inflater = (LayoutInflater) Home.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.settings,
                null,false);
        ImageButton settings = (ImageButton)findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final UserSettings u = getCreateOrUpdateMusicStatus(true,0,true,context);
                final PopupWindow pw = new PopupWindow(layout,(int) mScreenWidth,(int) mScreenHeight, true);
                final TextView gameSeekSpeed = (TextView)layout.findViewById(R.id.gameSeekSpeed);
                final SeekBar seekBar = (SeekBar)layout.findViewById(R.id.seekLevel);
                seekBar.incrementProgressBy(1);
                seekBar.setMax(2);
                if(u.getGameSpeed() > 6000L) {
                    seekBar.setProgress(0);
                    gameSeekSpeed.setText("Slow");
                    gameSeekSpeed.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
                else if (u.getGameSpeed() < 6000L){
                    gameSeekSpeed.setText("Fast");
                    seekBar.setProgress(2);
                    gameSeekSpeed.setTextColor(getResources().getColor(R.color.colorAccent));

                }
                else {
                    gameSeekSpeed.setText("Normal");
                    seekBar.setProgress(1);
                    gameSeekSpeed.setTextColor(getResources().getColor(R.color.range));
                }

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(progress == 0){
                            gameSeekSpeed.setText("Slow");
                            gameSeekSpeed.setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                        else if(progress == 1){
                            gameSeekSpeed.setText("Normal");
                            gameSeekSpeed.setTextColor(getResources().getColor(R.color.range));
                        }
                        else{
                            gameSeekSpeed.setText("Fast");
                            gameSeekSpeed.setTextColor(getResources().getColor(R.color.colorAccent));
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                final ImageButton closeSetting = (ImageButton)layout.findViewById(R.id.closeSetting);
                closeSetting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pw.dismiss();
                    }
                });

                ImageButton saveSettings = (ImageButton)layout.findViewById(R.id.savesettings);
                saveSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (seekBar.getProgress() == 0) getCreateOrUpdateMusicStatus(u.isMusicOn(),6000L + 1500L,false,context);
                        else if (seekBar.getProgress() == 1) getCreateOrUpdateMusicStatus(u.isMusicOn(),6000L,false,context);
                        else  getCreateOrUpdateMusicStatus(u.isMusicOn(),6000L - 2000L,false,context);
                        pw.dismiss();
                    }
                });

                final ImageButton soundOn = (ImageButton)layout.findViewById(R.id.soundOnSet);
                int color = Color.WHITE;
                Drawable dOn;
                if(getCreateOrUpdateMusicStatus(true,0,true,context).isMusicOn())
                    dOn = ContextCompat.getDrawable(Home.this,ic_lock_silent_mode_off);
                else
                    dOn = ContextCompat.getDrawable(Home.this,ic_lock_silent_mode);
                DrawableCompat.setTint(dOn, color);
                soundOn.setImageDrawable(dOn);
                soundOn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int color = Color.WHITE;
                        Drawable d ;
                        if(getCreateOrUpdateMusicStatus(true,0,true,context).isMusicOn()){
                            gameSound.pause();
                            d = ContextCompat.getDrawable(Home.this,ic_lock_silent_mode);
                            getCreateOrUpdateMusicStatus(false,DEFAULT_ANIMATION_DURATION,false,context);
                        }else {
                            gameSound.start();
                            d = ContextCompat.getDrawable(Home.this,ic_lock_silent_mode_off);
                            getCreateOrUpdateMusicStatus(true,DEFAULT_ANIMATION_DURATION,false,context);
                        }
                        DrawableCompat.setTint(d, color);
                        soundOn.setImageDrawable(d);
                    }
                });
                pw.showAtLocation(findViewById(R.id.container), Gravity.CENTER, 0, 0);
            }
        });

        final Intent intent = new Intent(Home.this,HelpActivity.class);
        ImageButton help = (ImageButton)findViewById(R.id.helpHome);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        mScreenWidth = displaymetrics.widthPixels;
        mScreenHeight = displaymetrics.heightPixels;
        if (getCreateOrUpdateMusicStatus(true,0,true,context).isMusicOn()) gameSound.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameSound.pause();
     }



}
