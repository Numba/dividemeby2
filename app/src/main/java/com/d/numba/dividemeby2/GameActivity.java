package com.d.numba.dividemeby2;


import android.animation.AnimatorSet;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.d.numba.dividemeby2.fragments.ButtonsViewFragment;
import com.d.numba.dividemeby2.fragments.ColorsViewFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Random;

import static android.R.drawable.ic_lock_silent_mode;
import static android.R.drawable.ic_lock_silent_mode_off;
import static com.d.numba.dividemeby2.FunctionsNeeded.animateBackground;
import static com.d.numba.dividemeby2.FunctionsNeeded.animateResult;
import static com.d.numba.dividemeby2.FunctionsNeeded.getCreateOrUpdateBestScore;
import static com.d.numba.dividemeby2.FunctionsNeeded.getCreateOrUpdateMusicStatus;
import static com.d.numba.dividemeby2.FunctionsNeeded.recreateActivityCompat;
import static com.d.numba.dividemeby2.FunctionsNeeded.showPopup;
import static com.d.numba.dividemeby2.Home.gameSound;


public class GameActivity extends AppCompatActivity {


    public static long DEFAULT_ANIMATION_DURATION;
    public static final long FAST_ANIMATION_DURATION = 500L;
    public static float goodAnswer = 0;
    public static float positionStart = 0;
    public static float bestScoreValue = 0;
    public static int lost = 0;
    public static boolean isNeeded = false;
    private static final int NUM_PAGES = 2;
    static final int MIN_DISTANCE = 150;

    private CustomBundleView bounce0;
    private RelativeLayout mFrameLayout;
    private float mScreenWidth, mScreenHeight;
    public static boolean fast = false;
    private TextView score;
    boolean validated = false;
    boolean divisible = false;
    boolean gotRight = false;
    private boolean sleep = false;
    Random rand = new Random();
    TextView response, bestScore;
    Context context;
    ImageButton pause;


    public void makeSleep() {
        sleep = true;
    }

    public void wakeUp() {
        sleep = false;
    }

    boolean isSleeping() {
        return this.sleep;
    }

    PopupWindow pw;
    private float x1;
    private ViewPager mPager;

    Intent intent;
    int cptIndivisible = 0;

    int foundForDifficulty = 0;
    View layout, layoutPause;
    int difficulty = 10;

    private InterstitialAd mInterstitialAd;
    private boolean done = false;


    public void quitOne() {
        done = true;
        lost++;
        showPopup(GameActivity.this, layout, intent, mInterstitialAd);
    }

    public boolean isDone() {
        return done;
    }

    /// @SuppressLint("InflateParams")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        context = this;
        score = (TextView) findViewById(R.id.score);
        mFrameLayout = (RelativeLayout) findViewById(R.id.container);
        response = (TextView) findViewById(R.id.result);
        mFrameLayout = (RelativeLayout) findViewById(R.id.container);
        bounce0 = new CustomBundleView(findViewById(R.id.buble));
        bestScore = (TextView) findViewById(R.id.bestScore);
        pause = (ImageButton) findViewById(R.id.pause);

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupPause();
            }
        });

        if (getCreateOrUpdateBestScore(0, true, context) != null) {
            bestScoreValue = getCreateOrUpdateBestScore(0, true, context).getValue();
        }
        bestScore.setText("" + bestScoreValue);

        bounce0.setBackgroundBubble(this);

        initializeBundle0();

        LayoutInflater inflater = (LayoutInflater) GameActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.popup_lost,
                null, false);
        layoutPause = inflater.inflate(R.layout.on_paused_popup,
                null, false);

        intent = new Intent(GameActivity.this, Home.class);

        response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPager.getCurrentItem() == 1) {
                    int bundleValue = Integer.parseInt(((TextView) getBounce0().getView()).getText().toString());
                    int resultValue = Integer.parseInt(response.getText().toString());
                    getBounce0().getAnimatorSetDefault().cancel();
                    if (bundleValue / 2 == resultValue && (bundleValue != 0 && bundleValue != 1)) {
                        gotRight = true;
                        validated = true;
                        if (foundForDifficulty == 2) {
                            difficulty = difficulty + 50;
                            foundForDifficulty = 0;
                        }
                    } else {
                        gotRight = false;
                        getCreateOrUpdateBestScore(goodAnswer, false, context);
                    }
                    animateResult(response, getBounce0(), context, getScreenWidth(), getFrameLayout(), getScore(), gotRight, layout, GameActivity.this, intent, true, mInterstitialAd);
                }
            }

        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getBounce0().animateBundle(getScreenWidth(), DEFAULT_ANIMATION_DURATION, GameActivity.this);
            }
        }, 1020);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    start();
                    Thread.sleep(1000);
                    if (positionStart == 0)
                        positionStart = getBounce0().getView().getX();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        DEFAULT_ANIMATION_DURATION = getCreateOrUpdateMusicStatus(true, 0, true, this).getGameSpeed();
        mPager = (NonSwipeViewPager) findViewById(R.id.pager);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPager.setCurrentItem(0);
        // MobileAds.initialize(this, getResources().getString(R.string.appIdPub));
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.intertitiel));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

    }
    public void start() {
        ((new BundleTask())).execute(getBounce0());
    }

    @Override
    protected void onResume() {
        super.onResume();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        mScreenWidth = displaymetrics.widthPixels;
        mScreenHeight = displaymetrics.heightPixels;
        if (getCreateOrUpdateMusicStatus(true, 0, true, context).isMusicOn()) gameSound.start();
    }

    public ViewPager getViewPager() {
        return this.mPager;
    }

    public CustomBundleView getBounce0() {
        return bounce0;
    }

    public float getScreenWidth() {
        return mScreenWidth;
    }

    public RelativeLayout getFrameLayout() {
        return mFrameLayout;
    }

    public void showPopupPause() {
        try {
            pw = new PopupWindow(layoutPause, (int) mScreenWidth, (int) mScreenHeight, true);
            getBounce0().getAnimatorSetDefault().cancel();
            getBounce0().getAnimatorSetFast().cancel();
            ImageButton restart = (ImageButton) layoutPause.findViewById(R.id.restart);
            restart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quitOne();
                    recreateActivityCompat(GameActivity.this);
                }
            });
            ImageButton home = (ImageButton) layoutPause.findViewById(R.id.quitPop);
            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(intent);
                    finish();
                }
            });
            ImageButton resumePause = (ImageButton) layoutPause.findViewById(R.id.resumePause);
            resumePause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(fast){
                        getBounce0().setAnimatorSetFast(new AnimatorSet());
                        getBounce0().resumeAnimationBundle((positionStart + (-getBounce0().getView().getX())), mScreenWidth,FAST_ANIMATION_DURATION);
                        fast = false;
                    }else {
                        getBounce0().setAnimatorSetDefault(new AnimatorSet());
                        getBounce0().resumeAnimationBundle((positionStart + (-getBounce0().getView().getX())), mScreenWidth, DEFAULT_ANIMATION_DURATION);
                    }
                    pw.dismiss();
                }
            });
            final ImageButton stopSound = (ImageButton) layoutPause.findViewById(R.id.stopMusic);
            int color = Color.GRAY;
            Drawable dOn;
            if (getCreateOrUpdateMusicStatus(true, 0, true, context).isMusicOn())
                dOn = ContextCompat.getDrawable(GameActivity.this, ic_lock_silent_mode_off);
            else
                dOn = ContextCompat.getDrawable(GameActivity.this, ic_lock_silent_mode);
            DrawableCompat.setTint(dOn, color);
            stopSound.setImageDrawable(dOn);
            stopSound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int color = Color.GRAY;
                    Drawable d;
                    if (getCreateOrUpdateMusicStatus(true, 0, true, context).isMusicOn()) {
                        gameSound.pause();
                        d = ContextCompat.getDrawable(GameActivity.this, ic_lock_silent_mode);
                        getCreateOrUpdateMusicStatus(false, DEFAULT_ANIMATION_DURATION, false, context);
                    } else {
                        gameSound.start();
                        d = ContextCompat.getDrawable(GameActivity.this, ic_lock_silent_mode_off);
                        getCreateOrUpdateMusicStatus(true, DEFAULT_ANIMATION_DURATION, false, context);
                    }
                    DrawableCompat.setTint(d, color);
                    stopSound.setImageDrawable(d);
                }
            });

            pw.showAtLocation(findViewById(R.id.container), Gravity.CENTER, 0, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        if (pw != null)
            if (!pw.isShowing())
                showPopupPause();
    }

    public TextView getScore() {
        return score;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return new ColorsViewFragment();
            else
                return new ButtonsViewFragment();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public void initializeBundle0() {
        int value = rand.nextInt(difficulty);
        if (value % 2 == 0 && value != 0) divisible = true;
        ((TextView) getBounce0().getView()).setText("" + (value));
    }

    private class BundleTask extends AsyncTask<CustomBundleView, CustomBundleView, Void> {


        @Override
        public void onPreExecute() {

        }

        @Override
        protected Void doInBackground(CustomBundleView... arg0) {

            CustomBundleView v = arg0[0];
            while (!done) {
                try {
                    Thread.sleep(100);
                    if (!isSleeping())
                        publishProgress(v);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(final CustomBundleView... values) {
            super.onProgressUpdate(values);
            float position = (positionStart - values[0].getView().getX());
            if (position == getScreenWidth()) {
                makeSleep();
                float bundleValue = Float.parseFloat(((TextView) getBounce0().getView()).getText().toString());
                float resultValue = Float.parseFloat(response.getText().toString());
                if ((resultValue == (bundleValue / 2)) && bundleValue != 0 && bundleValue != 1) {
                    foundForDifficulty++;
                    if (!validated) {
                        goodAnswer = goodAnswer + 1;
                        isNeeded = true;
                        //getScore().animate();
                        score.setText("" + goodAnswer);
                    }
                    if (foundForDifficulty == 3) {
                        difficulty = difficulty + 50;
                        foundForDifficulty = 0;
                    }
                    validated = true;
                }
                if (bundleValue % 2 != 0 && resultValue > 0) {
                    quitOne();
                }
                if (getViewPager().getCurrentItem() == 0) {
                    isNeeded = true;
                    goodAnswer = goodAnswer - (float) (0.5);

                }
                getFrameLayout().removeView(values[0].getView());
                if (divisible && !validated) {
                    getCreateOrUpdateBestScore(goodAnswer, false, context);
                    quitOne();
                }

                int randomNum = rand.nextInt(difficulty);
                if (randomNum % 2 == 0 && randomNum != 0) divisible = true;
                else if (randomNum % 2 != 0 || randomNum == 0) {
                    divisible = false;
                    if (cptIndivisible < 2) {
                        cptIndivisible++;
                    } else {
                        randomNum = randomNum + 2;
                        if (randomNum % 2 != 0) randomNum = randomNum + 1;
                        cptIndivisible = 0;
                    }
                }
                validated = false;
                ((TextView) getBounce0().getView()).setText("" + randomNum);

                if (!isDone()) {
                    getFrameLayout().addView(getBounce0().getView());
                    response.setText("" + 0);
                    getBounce0().setBackgroundBubble(context);
                    getViewPager().setCurrentItem(0);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getBounce0().animateBundle(getScreenWidth(), DEFAULT_ANIMATION_DURATION, GameActivity.this);
                        animateBackground(FAST_ANIMATION_DURATION, rand, context, getFrameLayout());
                        wakeUp();
                    }
                }, 100);
            }
        }

        @Override
        protected void onPostExecute(Void result) {
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    if (getViewPager().getCurrentItem() == 1) {
                        getBounce0().animateBundleFaster((positionStart - getBounce0().getView().getX()), getScreenWidth());
                        int resultValue = Integer.parseInt(((TextView) getBounce0().getView()).getText().toString());
                        if (resultValue % 2 == 0 && resultValue != 0) {
                            quitOne();
                        }
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pw != null) {
            if (pw.isShowing()) pw.dismiss();
        }
        done = true;
        goodAnswer = 0;
    }


    @Override
    protected void onPause() {
        super.onPause();
        gameSound.pause();
        showPopupPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (getCreateOrUpdateMusicStatus(true, 0, true, context).isMusicOn()) gameSound.start();
    }
}
