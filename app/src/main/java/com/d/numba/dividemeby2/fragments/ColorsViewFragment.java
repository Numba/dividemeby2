package com.d.numba.dividemeby2.fragments;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.d.numba.dividemeby2.HelpActivity;
import com.d.numba.dividemeby2.R;
import com.d.numba.dividemeby2.GameActivity;

import static com.d.numba.dividemeby2.FunctionsNeeded.animateColor;
import static com.d.numba.dividemeby2.FunctionsNeeded.setColorBackground;


/**
 * A simple {@link Fragment} subclass.
 */
public class ColorsViewFragment extends Fragment {


    public ColorsViewFragment() {
        // Required empty public constructor
    }

    ViewPager viewPager;
    String from;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_down_items_container, container, false);
        LinearLayout colorsContainer = (LinearLayout)rootView.findViewById(R.id.itemContainer);
        if(getActivity().getClass().equals(GameActivity.class)) {
            viewPager = ((GameActivity) getActivity()).getViewPager();
            from = "ga";
        }
        else {
            viewPager = ((HelpActivity) getActivity()).getPager();
            from = "he";
        }
        for(int i = 0 ; i < 4 ; i++){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            params.setMargins(5, 5, 5, 5);
            final ImageButton imageButton = new ImageButton(getActivity());
            imageButton.setLayoutParams(params);
            setColorBackground(imageButton,getActivity());
            colorsContainer.addView(imageButton);
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(from.equals("ga"))
                        animateColor(imageButton,((GameActivity)getActivity()).getBounce0(),getActivity(),((GameActivity)getActivity()).getFrameLayout(),getActivity());
                    else
                        animateColor(imageButton,((HelpActivity)getActivity()).getBubble0(),getActivity(),((HelpActivity)getActivity()).getFrameLayout(),getActivity());
                }
            });
        }


        return  rootView;
    }

}
